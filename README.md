
# Mômji Frontend projet
Projet qui permet à l'utilisateur d'envoyer des fichiers depuis un formulaire et de simuler leur envoi en utilisant la librairie React-dropzone.


# Installation
Utilisez la commande composer afin d'installer les dépendances du projet.
> npm i


Démarrer l'application
> npm run start
