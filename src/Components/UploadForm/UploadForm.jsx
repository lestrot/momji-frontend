import {useState} from "react";
import {useDropzone} from "react-dropzone";

export default function UploadForm() {
    const [files, setFiles] = useState([])
    const [isSending, setIsSending] = useState(false)
    const [progress, setProgress] = useState(0)

    const handleDrop = (acceptedFiles) => {
        setFiles([...files, ...acceptedFiles])
    }

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop: handleDrop,
        accept: '*',
        maxFiles: 5,
    })

    const fileList = files.map((file, index) => (
        <li key={index}>{file.name}</li>
    ))

    const handleSend = () => {
        setIsSending(true);
        setProgress(0);
        setTimeout(() => {
            setIsSending(false);
            setProgress(0);
            setFiles([]);
        }, 30000);
        const interval = setInterval(() => {
            if (progress >= 100) {
                clearInterval(interval);
            } else {
                setProgress(progress + 10);
            }
        }, 200);
    };

    return (
        <>
            { isSending ? (
                <div className={'progress'}>
                    {/*<progress value={progress} max="100" />*/}
                    <progress value={progress.current} max="100" />
                    <p>Sending {files.length} file(s)...</p>
                </div>
            ) :
        <div className={'form'}>
            <div {...getRootProps()}>
                <input {...getInputProps()} />
                {isDragActive ? (
                    <p>Drop the files here ...</p>
                ) : (
                    <p>Drag 'n' drop some files here, or click to select files</p>
                )}
            </div>
            <ul>{fileList}</ul>
            <button onClick={handleSend} disabled={isSending || files.length === 0}>
                Send
            </button>
        </div>
            }
        </>
    )
}