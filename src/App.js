import './App.css';
import UploadForm from "./Components/UploadForm/UploadForm";

function App() {
  return (
    <div className={"App"}>
        <h1>File sending form</h1>
        <UploadForm/>
    </div>
  )
}

export default App;
